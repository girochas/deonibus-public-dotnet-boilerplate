﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DeOnibus.Domain.DTOs
{
    public class ItinerationDTO
    {
        public List<ResultsDto> Results { get; set; }

    }

    public class ResultsDto
    {

        [JsonProperty(PropertyName = "objectId")]
        public string Id { get; set; }
        public CompanyDTO Company { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public DateDTO DepartureDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public decimal Price { get; set; }
        public string BusClass { get; set; }
        public DateDTO ArrivalDate { get; set; }
    }

    public class DateDTO
    {
        [JsonProperty(PropertyName = "__type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "iso")]
        public DateTime Date { get; set; }
    }

    public class CompanyDTO
    {
        public string Name { get; set; }
    }
}
