﻿using DeOnibus.Domain.DTOs;
using DeOnibus.Domain.Entities;
using DeOnibus.Domain.Entities.Data;
using System.Threading.Tasks;

namespace DeOnibus.Domain.Interfaces.Services
{
    public interface IDeOnibusService
    {
        Task<ItinerationDTO> GetItinerations(int accountId);
        Task<ItinerationDTO> GetFavoritesItinerations(int accountId);
        Task CreateFavoriteItineration(FavoriteItineration favoriteItineration);
        Task RemoveFavoriteItineration(FavoriteItineration favoriteItineration);
    }
}
