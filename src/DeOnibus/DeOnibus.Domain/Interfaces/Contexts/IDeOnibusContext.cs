﻿using DeOnibus.Domain.Entities.Data;
using Microsoft.EntityFrameworkCore;

namespace DeOnibus.Domain.Interfaces.Contexts
{
    public interface IDeOnibusContext: IDbContext
    {
        DbSet<Account> Accounts { get; set; }
        DbSet<FavoriteItineration> FavoriteItinerations { get; set; }
    }
}
