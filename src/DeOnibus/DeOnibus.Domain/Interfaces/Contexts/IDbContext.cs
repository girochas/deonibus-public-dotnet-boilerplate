﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace DeOnibus.Domain.Interfaces.Contexts
{
    public interface IDbContext
    {
        EntityEntry Add(object entity);
        DbSet<T> Set<T>() where T : class;
        int SaveChanges();
    }
}
