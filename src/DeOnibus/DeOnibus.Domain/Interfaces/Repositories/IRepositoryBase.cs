﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibus.Domain.Interfaces.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        void Add(T obj);
        T GetById(object id);
        IEnumerable<T> GetAll();
        void Update(T obj);
        void Remove(T obj);
        void Dispose();
        
    }
}
