﻿using DeOnibus.Domain.Entities.Data;

namespace DeOnibus.Domain.Interfaces.Repositories
{
    public interface IAccountRepository: IRepositoryBase<Account>
    {
    }
}
