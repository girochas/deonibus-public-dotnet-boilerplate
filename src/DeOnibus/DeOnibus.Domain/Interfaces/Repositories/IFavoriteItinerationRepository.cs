﻿using DeOnibus.Domain.Entities.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibus.Domain.Interfaces.Repositories
{
    public interface IFavoriteItinerationRepository: IRepositoryBase<FavoriteItineration>
    {
        FavoriteItineration GetByAccountIdAndItinearationId(int accountId, string intinerationId);
        List<FavoriteItineration> GetByAccountId(int accountId);
    }
}
