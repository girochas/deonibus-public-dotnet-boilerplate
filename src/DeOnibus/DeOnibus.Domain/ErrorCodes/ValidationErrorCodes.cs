﻿namespace DeOnibus.Domain.ErrorCodes
{
    public class ValidationErrorCodes
    {
        public static string ItinerationNotFound => "O Id do intinerario encontrato";
        public static string AccountNotFound => "Conta não existente";
        public static string FavoriteItinerationAlredyExist => "Esse intineratirio já é favorito";
        public static string FavoriteItinerationNotFound => "Intineratio não pode ser removido dos favoridos, pois não favorito";
    }
}
