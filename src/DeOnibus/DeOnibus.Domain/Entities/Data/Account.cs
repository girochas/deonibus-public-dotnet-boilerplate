﻿using System.Collections.Generic;

namespace DeOnibus.Domain.Entities.Data
{
    public class Account
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<FavoriteItineration> FavoriteItineration { get; set; }
    }
}
