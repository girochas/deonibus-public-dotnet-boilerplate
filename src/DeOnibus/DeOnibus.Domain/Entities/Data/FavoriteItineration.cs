﻿namespace DeOnibus.Domain.Entities.Data
{
    public class FavoriteItineration
    {
        public int Id { get; set; }
        public string ItinerationId { get; set; }
        public int AccountId { get; set; }

    }
}