﻿using System;

namespace DeOnibus.Domain.Entities
{
    public class DeOnibusResponse : BaseResponse
    {
        public string Id { get; set; }
        public CompanyResponse Company { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public DateResponse DepartureDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public decimal Price { get; set; }
        public string BusClass { get; set; }
        public DateResponse ArrivalDate { get; set; }
        public bool IsBestPrice { get; set; }
    }


    public class DateResponse
    {
        public string Type { get; set; }
        public DateTime DateAt { get; set; }
    }

    public class CompanyResponse
    {
        public string Name { get; set; }
    }


}
