﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibus.Domain.Entities
{
    public class BaseResponse
    {
        public bool Success { get; set; }
        public string Mensage { get; set; }


        public static BaseResponse Fail(string mensage)
        {
            return new BaseResponse
            {
                Success= false,
                Mensage = mensage
            };
        }
    }


}
