﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibus.Domain.Entities
{
    public class FavoriteItinerationRequest
    {
        public string ItinerationId { get; set; }
        public int AccountId { get; set; }
    }
}
