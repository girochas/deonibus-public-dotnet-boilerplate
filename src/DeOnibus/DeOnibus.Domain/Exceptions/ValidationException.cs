﻿using System;

namespace DeOnibus.Domain.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException(string message): base(message)
        {
        }
    }
}
