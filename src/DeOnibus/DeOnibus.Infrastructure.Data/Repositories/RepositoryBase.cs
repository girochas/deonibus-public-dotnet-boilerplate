﻿using DeOnibus.Domain.Interfaces.Contexts;
using DeOnibus.Domain.Interfaces.Repositories;
using DeOnibus.Infrastructure.Data.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibus.Infrastructure.Data.Repositories
{
    public class RepositoryBase<T> : IDisposable, IRepositoryBase<T> where T : class
    {
        protected IDeOnibusContext context;

        public RepositoryBase(IDeOnibusContext context)
        {
            this.context = context;
        }

        public IEnumerable<T> GetAll() => context.Set<T>().AsNoTracking();
        public T GetById(object id) => context.Set<T>().Find(id);

        public void Add(T obj)
        {
            context.Set<T>().Add(obj);
            context.SaveChanges();
        }

        public void Remove(T obj)
        {
            context.Set<T>().Remove(obj);
            context.SaveChanges();
        }

        public void Update(T obj)
        {
            context.Set<T>().Update(obj);
            context.SaveChanges();
        }

        public void Dispose()
        {
           
        }        
    }
}
