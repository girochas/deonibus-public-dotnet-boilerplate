﻿using DeOnibus.Domain.Entities.Data;
using DeOnibus.Domain.Interfaces.Contexts;
using DeOnibus.Domain.Interfaces.Repositories;

namespace DeOnibus.Infrastructure.Data.Repositories
{
    public class AccountRepository : RepositoryBase<Account>, IAccountRepository
    {
        public AccountRepository(IDeOnibusContext context) : base(context)
        {
        }
    }
}
