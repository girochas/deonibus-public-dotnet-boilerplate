﻿using DeOnibus.Domain.Entities.Data;
using DeOnibus.Domain.Interfaces.Contexts;
using DeOnibus.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeOnibus.Infrastructure.Data.Repositories
{
    public class FavoriteItinerationRepository : RepositoryBase<FavoriteItineration>, IFavoriteItinerationRepository
    {
        public FavoriteItinerationRepository(IDeOnibusContext context) : base(context)
        {
        }

        public List<FavoriteItineration> GetByAccountId(int accountId)
        {
            return context.FavoriteItinerations.Where(fi => fi.AccountId == accountId).ToList();
        }

        public FavoriteItineration GetByAccountIdAndItinearationId(int accountId, string itinerationId)
        {
            return context.FavoriteItinerations.FirstOrDefault(fi => fi.AccountId == accountId && fi.ItinerationId == itinerationId);
        }
    }
}
