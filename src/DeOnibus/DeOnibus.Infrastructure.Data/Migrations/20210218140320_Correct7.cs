﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeOnibus.Infrastructure.Data.Migrations
{
    public partial class Correct7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RouteId",
                table: "FavoriteItinerations",
                newName: "ItinerationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ItinerationId",
                table: "FavoriteItinerations",
                newName: "RouteId");
        }
    }
}
