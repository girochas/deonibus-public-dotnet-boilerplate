﻿using DeOnibus.Domain.Entities.Data;
using DeOnibus.Domain.Interfaces.Contexts;
using DeOnibus.Infrastructure.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibus.Infrastructure.Data.Contexts
{
    public class DeOnibusContext : DbContext, IDeOnibusContext
    {
        protected readonly string ConnectionString;
        protected readonly IDeOnibusContext context;

        //public DeOnibusContext(IOptions<ConnectionStringConfiguration> options,
        //                       IDeOnibusContext context)
        //{
        //    ConnectionString = options.Value.Default;
        //    this.context = context;
        //}

        public DeOnibusContext()
        {
            ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=DeOnibus;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>().HasData(new Account
            {
                Id = 1,
                Name = "Teste 1"
            });
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<FavoriteItineration> FavoriteItinerations { get; set; }
    }
}
