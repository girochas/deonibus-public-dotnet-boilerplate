using DeOnibus.Api.Middlewares;
using DeOnibus.Api.StartUpConfigs;
using DeOnibus.Infrastructure.Configurations;
using DeOnibus.Infrastructure.Profiles;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DeOnibus.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ConnectionStringConfiguration>(options => Configuration.GetSection("ConnectionStrings").Bind(options));
            services.Configure<DeOnibusConfiguration>(options => Configuration.GetSection("DeOnibus").Bind(options));
            
            services.AddControllers();
            services.AddInternalServices();
            
            var configs = Configuration.GetSection("DeOnibus").Get<DeOnibusConfiguration>();
            services.AddDeOnibusApi(deOnibusApiUrl: configs.Api.Url);

            services.AddAutoMapper(typeof(FavoriteItinerationProfile).Assembly);
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();

            app.UseMiddleware(typeof(ExceptionHandlerMiddleware));

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
