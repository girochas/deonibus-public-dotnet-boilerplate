﻿using AutoMapper;
using DeOnibus.Domain.Entities;
using DeOnibus.Domain.Entities.Data;
using DeOnibus.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DeOnibus.Api.Controllers
{
    [Route("api/[controller]")]
    public class ItinerationController : Controller
    {
        private readonly IDeOnibusService deOnibusService;
        private readonly IMapper mapper;

        public ItinerationController(IDeOnibusService deOnibusService,
                                     IMapper mapper)
        {
            this.deOnibusService = deOnibusService;
            this.mapper = mapper;
        }

        [HttpGet, Route("{accountId}")]
        public async Task<IActionResult> Get(int accountId)
        {
            var itinerations = await deOnibusService.GetItinerations(accountId);
            return Ok(itinerations.Results);
        }

        [HttpGet, Route("{accountId}/favorite")]
        public async Task<IActionResult> GetFavoritesItineration(int accountId)
        {
            var itinerations = await deOnibusService.GetFavoritesItinerations(accountId);
            return Ok(itinerations.Results);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]FavoriteItinerationRequest request)
        {
            await deOnibusService.CreateFavoriteItineration(mapper.Map<FavoriteItineration>(request));

            return Created(string.Empty, null);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]FavoriteItinerationRequest request)
        {
            await deOnibusService.RemoveFavoriteItineration(mapper.Map<FavoriteItineration>(request));

            return Created(string.Empty, null);
        }
    }
}
