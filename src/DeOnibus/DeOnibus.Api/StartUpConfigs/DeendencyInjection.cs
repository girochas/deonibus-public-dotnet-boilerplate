﻿using DeOnibus.Domain.Interfaces.Contexts;
using DeOnibus.Domain.Interfaces.Repositories;
using DeOnibus.Domain.Interfaces.Services;
using DeOnibus.Infrastructure.ApiContracts;
using DeOnibus.Infrastructure.Data.Contexts;
using DeOnibus.Infrastructure.Data.Repositories;
using DeOnibus.Services.Implematations;
using Microsoft.Extensions.DependencyInjection;
using Refit;
using System;

namespace DeOnibus.Api.StartUpConfigs
{
    public static class DeendencyInjection
    {
        public static IServiceCollection AddInternalServices(this IServiceCollection services)
        {
            services.AddScoped<IDeOnibusContext, DeOnibusContext>();
            services.AddScoped<IFavoriteItinerationRepository, FavoriteItinerationRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IDeOnibusService, DeOnibusService>();

            return services;
        }

        public static void AddDeOnibusApi(this IServiceCollection services, string deOnibusApiUrl)
        {
            services.AddRefitClient<IDeOnibusApi>(new RefitSettings { ContentSerializer = new NewtonsoftJsonContentSerializer() })
                    .ConfigureHttpClient(c => c.BaseAddress = new Uri(deOnibusApiUrl));

        }

    }
}
