﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibus.Infrastructure.Configurations
{
    public class DeOnibusConfiguration
    {
        public Api Api { get; set; }
    }


    public class Api
    {
        public string Url { get; set; }
    }
}
