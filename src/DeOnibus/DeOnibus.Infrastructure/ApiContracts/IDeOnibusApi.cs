﻿using DeOnibus.Domain.DTOs;
using Refit;
using System.Threading.Tasks;

namespace DeOnibus.Infrastructure.ApiContracts
{
    public interface IDeOnibusApi
    {
        [Get("/route")]
        Task<ItinerationDTO> GetRoutes();
    }
}
