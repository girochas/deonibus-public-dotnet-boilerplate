﻿using AutoMapper;
using DeOnibus.Domain.Entities;
using DeOnibus.Domain.Entities.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibus.Infrastructure.Profiles
{
    public class FavoriteItinerationProfile: Profile
    {
        public FavoriteItinerationProfile()
        {
            CreateMap<FavoriteItinerationRequest, FavoriteItineration>().ReverseMap();           
        }
    }
}
