﻿using DeOnibus.Domain.DTOs;
using DeOnibus.Domain.Entities;
using DeOnibus.Domain.Entities.Data;
using DeOnibus.Domain.ErrorCodes;
using DeOnibus.Domain.Exceptions;
using DeOnibus.Domain.Interfaces.Repositories;
using DeOnibus.Domain.Interfaces.Services;
using DeOnibus.Infrastructure.ApiContracts;
using Refit;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeOnibus.Services.Implematations
{
    public class DeOnibusService : IDeOnibusService
    {
        private readonly IDeOnibusApi deOnibusApi;
        private readonly IFavoriteItinerationRepository favoriteItinerationRepository;
        private readonly IAccountRepository accountRepository;

        public DeOnibusService(IDeOnibusApi deOnibusApi,
                               IFavoriteItinerationRepository favoriteItinerationRepository,
                               IAccountRepository accountRepository)
        {
            this.deOnibusApi = deOnibusApi;
            this.favoriteItinerationRepository = favoriteItinerationRepository;
            this.accountRepository = accountRepository;
        }


        public async Task<ItinerationDTO> GetItinerations(int accountId)
        {
            var itinerationsResult = new ItinerationDTO { Results = new List<ResultsDto>() };

            var itinerations = await GetAllItineration();

            var favoriteItinerationInDb = favoriteItinerationRepository.GetByAccountId(accountId);

            foreach (var itineration in itinerations.Results)
                if (favoriteItinerationInDb.Any(fi => fi.ItinerationId == itineration.Id) == false)
                    itinerationsResult.Results.Add(itineration);

            return itinerationsResult;
        }

        public async Task<ItinerationDTO> GetFavoritesItinerations(int accountId)
        {
            var itinerationsResult = new ItinerationDTO { Results = new List<ResultsDto>() };
            var itinerations = await GetAllItineration();
            var itinerationToRemode = new List<ResultsDto>();

            var favoriteItinerationInDb = favoriteItinerationRepository.GetByAccountId(accountId);

            foreach (var itineration in itinerations.Results)
                if (favoriteItinerationInDb.Any(fi => fi.ItinerationId == itineration.Id))
                    itinerationsResult.Results.Add(itineration);

            return itinerationsResult;
        }

        public async Task CreateFavoriteItineration(FavoriteItineration favoriteItineration)
        {
            await ValidationFavoriteItineration(favoriteItineration);

            if (favoriteItinerationRepository.GetByAccountIdAndItinearationId(favoriteItineration.AccountId, favoriteItineration.ItinerationId) != null)
                throw new ValidationException(ValidationErrorCodes.FavoriteItinerationAlredyExist);

            favoriteItinerationRepository.Add(favoriteItineration);
        }

        public async Task RemoveFavoriteItineration(FavoriteItineration favoriteItineration)
        {
            await ValidationFavoriteItineration(favoriteItineration);

            if (favoriteItinerationRepository.GetByAccountIdAndItinearationId(favoriteItineration.AccountId, favoriteItineration.ItinerationId) == null)
                throw new ValidationException(ValidationErrorCodes.FavoriteItinerationNotFound);

            favoriteItinerationRepository.Remove(favoriteItineration);
        }

        private async Task ValidationFavoriteItineration(FavoriteItineration favoriteItineration)
        {
            var itinerations = await GetAllItineration();

            if (accountRepository.GetById(favoriteItineration.AccountId) == null)
                throw new ValidationException(ValidationErrorCodes.AccountNotFound);

            if (itinerations.Results.Any(r => r.Id == favoriteItineration.ItinerationId) == false)
                throw new ValidationException(ValidationErrorCodes.ItinerationNotFound);

        }
        private async Task<ItinerationDTO> GetAllItineration()
        {
            try
            {
                var itinerations = await deOnibusApi.GetRoutes();
                itinerations.Results = itinerations.Results.OrderBy(r => r.DepartureDate.Date).ToList();
                return itinerations;
            }
            catch (ApiException exception)
            {
                throw;
            }
        }


    }
}
